% a.kolcun 2013 -*- mode: octave -*-
% kubicky interpolacny polynom - Newtonova forma
%_______________________________________________

echo off;
clear;

% definovanie koncovych bodov
%  P1  P2  P3  P4
X=[-2,  0,  3,  4];
Y=[-8,  0, 12, 40];

% diskretizacia
m=40; d=(X(4)-X(1))/m; x=X(1):d:X(4);

% interpolacny polynom - koeficienty v Newtonovej forme
a1 = Y(1);

a2 = Y(2)-a1;  q =    X(2)-X(1);   a2 = a2/q;

a3 = Y(3)-a1;  q =    X(3)-X(1);
a3 = a3-a2*q;  q = q*(X(3)-X(2));  a3 = a3/q;

a4 = Y(4)-a1;  q =    X(4)-X(1);
a4 = a4-a2*q;  q = q*(X(4)-X(2));
a4 = a4-a3*q;  q = q*(X(4)-X(3));  a4 = a4/q;

% interpolacny polynom - hodnoty pre zadanu diskretizaciu
for i=1:m+1,
    xi  = x(i);     xx = 1;
    yi  = a1;       xx = xx*(xi-X(1));
    yi  = yi+a2*xx; xx = xx*(xi-X(2));
    yi  = yi+a3*xx; xx = xx*(xi-X(3));
    yi  = yi+a4*xx;
    y(i)= yi;
end;

% vykreslenie
plot(X,Y,'ko',x,y,'b-');
