%_______________________________________________
% a.kolcun 2009 -*- mode: octave -*-
%
% parametric vs. geometric smoothness:
% view through red window - geometric smoothness

echo off;
hold off;
clear;
t1=-1; t2=1;
h  = 0.05;               % discretization

T=t1:h:t2; l=length(T);

% axes
tt=[t1 t2];  xt=[ 0  0];  yt=[ 0  0];
tx=[ 0  0];  xx=[t1 t2];  yx=[ 0  0];
ty=[ 0  0];  xy=[ 0  0];  yy=[t1 t2];

% window of geometric view (parametric projection)
wt=[t2 t2 t2 t2 t2];
wx=[t1 t2 t2 t1 t1];
wy=[t1 t1 t2 t2 t1];

% plot asis and labels
for i=1:2,
    figure(i);
    hold on;
    axis([t1 t2  t1 t2  t1 t2]);
    set(gca,'XTick',[t1 t2]);
    set(gca,'YTick',[t1 t2]);
    set(gca,'ZTick',[t1 t2]);
    grid on;
    plot3(tt,tx,ty,'k-',xt,xx,xy,'k-',yt,yx,yy,'k-');  % axis
    plot3(wt,wx,wy,'r-');                              % window
    xlabel('T'); ylabel('X'); zlabel('Y');
end;

% parametric smooth  -  geometric non-smooth
X=T.*T;
Y=T.*X;
figure(1);
% hold on;
plot3(T,X,Y,'b-','linewidth',3);                   % curve
plot3(T,X,Y,'g.','linewidth',1);

% parametric not smooth - geometric smooth
% 3D polyline
tP = 0.3;                % non-smooth point
a1 = 2;                  % slope
a2 = (a1*tP-1)/(tP-1);
b2 = 1-a2;
ll=int8(((l-1)-int8(2*tP/h))/2);
A(1:l)=a1; A(1:(ll+1))=a2;  A((l-ll):l)=a2;
B(1:l)=0;  B(1:(ll+1))=-b2; B((l-ll):l)=b2;
X=A.*T + B;
Y=A.*T + B;

figure(2);
% hold on;
plot3(T,X,Y,'b-','linewidth',3);                   % curve
plot3(T,X,Y,'g.','linewidth',1);

% print f1 -dbmp256; 

% 
% [1] Kolcun,A.: Parametrické vyjadrenie kriviek a plôch.
%                Uèebný text KIP PøF OU, Ostrava 2008.
