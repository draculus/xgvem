% a.kolcun 2013 -*- mode: octave -*-
% prvy priklad generovania krivky v parametrickom tvare
%_______________________________________________


echo off;
clear;
close all;


% definovanie riadiacich bodov
%  P0  P1  P2
X=[ 1;  1;  0];
Y=[ 0;  1;  1];

% diskretizacia
m = 50;  d = 1/m;

t = -d;
for i=1:m+1,
    t=t+d; q=1/(1+t*t);  w=(1-t);
    w0=w*w*q; w1=2*w*t*q; w2=2*t*t*q;
    W=[w0,w1,w2];
    x(i)=W*X;
    y(i)=W*Y;
end;

hold on;
plot(x,y,'r-', 'linewidth', 3); % krivka
plot(X(1:3), Y(1:3), 'bo-');    % riadiace body
