# -*- mode: octave -*-

close all;
clear;

function res = q (t)
  res = 1 / (1 + t^2);
endfunction

function res = mt (t)
  res = 1 - t;
endfunction

function res = w0 (t)
  res = mt(t)^2 * q (t);
endfunction

function res = w1 (t)
  res = (2 * mt(t) * t) * q(t);
endfunction

function res = w2 (t)
  res = (2 * t^2) * q(t);
endfunction

function plot_line (s, X, Y)
  d = 1 / s;

  for i = 1:(s + 1),
    t = (i - 1) * d;
    W = [w0(t), w1(t), w2(t)];
    x(i) = W * X;
    y(i) = W * Y;
  end;

  hold on;
  plot (x, y, 'r-');
  plot (X, Y, 'bo');
endfunction
