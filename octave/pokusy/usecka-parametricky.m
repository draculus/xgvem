# -*- mode: octave -*-

close all;
clear;

X = [0; 2];
Y = [1; 5];
m = 10;

for i = 1:(m + 1)
  t = (i - 1) / m;
  x(i) = X(1) + t * (X(2) - X(1));
  y(i) = Y(1) + t * (Y(2) - Y(1));
end;

hold on;
plot (x, y, "r-");
plot (X(1:2), Y(1:2), "go");
